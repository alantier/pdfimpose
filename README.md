pdfimpose
=========

A very simple command line tool to perform imposition (generate
signatures or booklets) on a a PDF file. Released under the Apache
License, version 2.0.

Dependencies
============

* PHP
* make
* pdflatex (thanks to pdfpages, which does all the heavy lifting)

Usage
=====

~~~
pdfimpose booklet <papersize> <in.pdf> <out.pdf>
pdfimpose folio|fo|quarto|4to|octavo|8vo|16mo|32mo|64mo <papersize> <signature-size> <in.pdf> <out.pdf>
~~~

Demo
====

![](./demo/img/demo-orig.png) 🠊 ![](./demo/img/demo-8vo.png)

* [Original document](https://alantier.gitlab.io/pdfimpose/demo.pdf)
* [Booklet (one fold, one signature)](https://alantier.gitlab.io/pdfimpose/demo-booklet.pdf)
* [Folios (one fold, 4-page signatures)](https://alantier.gitlab.io/pdfimpose/demo-fo-4.pdf)
* [Folios (one fold, 16-page signatures)](https://alantier.gitlab.io/pdfimpose/demo-fo-16.pdf)
* [Quartos (two folds, 8-page signatures)](https://alantier.gitlab.io/pdfimpose/demo-4to-8.pdf)
* [Quartos (two folds, 16-page signatures)](https://alantier.gitlab.io/pdfimpose/demo-4to-8.pdf)
* [Octavos (three folds, 16-page signatures)](https://alantier.gitlab.io/pdfimpose/demo-8vo-16.pdf)


List of paper sizes
===================

For more details, see the [Memoir class manual (`memman.pdf`)](http://texdoc.net/texmf-dist/doc/latex/memoir/memman.pdf).

* a6paper, a5paper, a4paper, a3paper
* b6paper, b5paper, b4paper, b3paper
* mcrownvopaper, mlargecrownvopaper, mdemyvopaper, msmallroyalvopaper
* dbillpaper, statementpaper, executivepaper, letterpaper, oldpaper, legalpaper, ledgerpaper, broadsheetpaper
* pottvopaper, foolscapvopaper, crownvopaper, postvopaper, largecrownvopaper, largepostvopaper, smalldemyvopaper, demyvopaper, mediumvopaper, smallroyalvopaper, royalvopaper, superroyalvopaper, imperialvopaper
* ebook, landscape
